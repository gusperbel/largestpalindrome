package com.indigo.largePalinrome.control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.indigo.largePalinrome.modelo.Adapterpalindrome;


public class Controller {
	private static Adapterpalindrome adapter = new Adapterpalindrome(); 
	
    public void getPalindromes() {
    	int resp = 0;
    	int min = 100;
    	int max = 999;
    	HashMap<String, String> palindromesList = new HashMap<String, String>();
    	for (int mi = min; mi < max; mi++) {
    		for (int ma = max; ma > min; ma--) {
    			resp = (ma * mi);
    			if(isNumberPalindrome(resp+"") && resp <= adapter.getNumberMaximumN())
    				palindromesList.put((ma+"x"+mi), resp+"");
    		}
    	}
    	adapter.setPalindromes(palindromesList);
    }
    
	public boolean setCasesTest(String insertCases) {
		boolean resp = false;
		try {
			int casesTest = Integer.parseInt(insertCases);
			if(casesTest >= 1 && casesTest <= 100) {
				adapter.setNumberCasesT(casesTest);
				resp = true;
			}			
		}catch(Exception e) {
			
		}
		return resp;
	}
	
	public boolean setNumberTextN(String textNumber) {
		boolean resp = false;
			try {
				int txtNum = Integer.parseInt(textNumber);
				if(txtNum > 101101 && txtNum <= 1000000) {
					adapter.setNumberMaximumN(txtNum);
					resp = true;
				}
			}catch(Exception e) {
				
			}
		return resp;
	}
	
	public void searchLargestPalindrome() {
		HashMap<String, String> listPalindromes = adapter.getPalindromes();
		ArrayList<String> listOutMessage = adapter.getListOutPut();
		String[] largestPalindrome = Collections.max(listPalindromes.entrySet(), Map.Entry.comparingByValue()).toString().split("=");
		//String[0] -> Key -> Palindrome
		//String[1] -> Value -> Product
		String[] factors = largestPalindrome[0].split("x");
		StringBuilder messageOut = new StringBuilder();
		messageOut.append("Case ["+adapter.getActualCase()+"]: "+largestPalindrome[1]+"\n");
		messageOut.append(largestPalindrome[1] + " is product of "+factors[0]+" and "+factors[1]+".\n");
		listOutMessage.add(messageOut.toString());
		adapter.setListOutPut(listOutMessage);
		
	}
	
	public boolean isNumberPalindrome(String numberProduct) {
    	boolean isPalindrome = true;
    	if (numberProduct.length() % 2 == 0) {
    		for(int i=0, d=(numberProduct.length()-1); i < numberProduct.length(); i++, d--) {
    			char start = numberProduct.charAt(i);
    			char end = numberProduct.charAt(d);   			
    			if(!(start == end)) {    				
    				isPalindrome = false;
    				break;
    			}
        	}
    	}else {
    		isPalindrome = false;
    	}    	
    	return isPalindrome;
    }
	    
    public Adapterpalindrome getAdapter() {
    	return adapter;
    }
    
    public void setActualCase(int actualCase) {
    	adapter.setActualCase(actualCase);
    }
}
