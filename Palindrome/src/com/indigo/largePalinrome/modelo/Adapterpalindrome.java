package com.indigo.largePalinrome.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Adapterpalindrome implements Serializable{
	private static final long serialVersionUID = -1;
	public Adapterpalindrome() {
		
	}
	
	private HashMap<String, String> Palindromes;
	private int numberCasesT;
	private int numberMaximumN;
	private int actualCase;
	private ArrayList<String> listOutPut = new ArrayList<String>();
	public HashMap<String, String> getPalindromes() {
		return Palindromes;
	}
	public void setPalindromes(HashMap<String, String> palindromes) {
		Palindromes = palindromes;
	}
	public int getNumberCasesT() {
		return numberCasesT;
	}
	public void setNumberCasesT(int numberCasesT) {
		this.numberCasesT = numberCasesT;
	}
	public int getNumberMaximumN() {
		return numberMaximumN;
	}
	public void setNumberMaximumN(int numberMaximumN) {
		this.numberMaximumN = numberMaximumN;
	}
	public int getActualCase() {
		return actualCase;
	}
	public void setActualCase(int actualCase) {
		this.actualCase = actualCase;
	}
	public ArrayList<String> getListOutPut() {
		return listOutPut;
	}
	public void setListOutPut(ArrayList<String> listOutPut) {
		this.listOutPut = listOutPut;
	}
}
