package com.indigo.largePalinrome;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import com.indigo.largePalinrome.control.Controller;
import com.indigo.largePalinrome.modelo.Adapterpalindrome;

public class LargePalindrome {	
	static Controller controll = new Controller();
    public static void main(String args[]) throws Exception{
    	    	
    	InputStreamReader insert = new InputStreamReader(System.in);
    	BufferedReader buffer = new BufferedReader(insert);
    	boolean isOkNumberCases = false;
    	while(!isOkNumberCases) {
    		System.out.println("Insert the number of test cases, where...\n 1 <= T <= 100");   			
    		isOkNumberCases = controll.setCasesTest(buffer.readLine());
    		if(!isOkNumberCases)
    			System.out.println("Please enter only numbers. Try again...");
    		    			
    	}			
		System.out.println(" _________________________________________________________ ");
		
		Adapterpalindrome adapter = controll.getAdapter();
		int countCases = 0;		
		while(countCases < adapter.getNumberCasesT()) {			
			System.out.println("Case ["+(countCases+1)+"] : Insert a value for N, where ...\n 101101 < N < 1000000");
			boolean isValueNok =controll.setNumberTextN(buffer.readLine());
			if(isValueNok) {
				countCases++;
				controll.setActualCase(countCases);				
				controll.getPalindromes();
				controll.searchLargestPalindrome();
			}else {
				System.out.println("Please enter only numbers. Try again...");
			}
			
		}
		System.out.println(" _________________________________________________________ ");
		
		adapter = controll.getAdapter();
		for(String messageOut : adapter.getListOutPut()) {
			System.out.println(messageOut);
		}
		
    }
    
}